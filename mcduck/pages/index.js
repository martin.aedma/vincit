import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { MainView } from '../src/components/MainView'

export default function Home() {
  return (
    <MainView/>
  )
}
