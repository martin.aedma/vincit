// ADD_TIME_FROM is Calculated from 1577836800 - 1577829600, where first value is
// taken from vincit example API call where from date is 1.1.2020 transformed to unix timestamp
// Second value is taken as date 1.1.2020 to UNIX time via javascript code
// Math.floor(Date.parse(new Date(2020, 0, 1).toUTCString()) / 1000)
// Date's are same, but FROM values differ 7200 seconds (2 hours).
// I guess that Finland is in +2 hours from GMT0/UTC, so thats why 2 hours is added in Vincit example ?
//
// I apply same logic to TO date, once again vincit exmple has +3 hours compared to unix time I get from code
// ADD_TIME_TO = 1609376400 - 1609365600 ( = 10800 seconds / or 3 hours)
// There is this base +2 hours but then there is additional hour to make sure we get all neccessary information
//
// If I make API call without adding these hours (or only add one hour to TO date), I receive information from 365 days between
// 1.1.2020 - 31.12.2020. If I add +2 hours to FROM date and +3 hours to TO date I get same values as in Vincit example, which is 366.
// That is why, in code I get UNIX timestamps and add +2 / +3 hours to FROM and TO dates accodrindgly.
// Hence why these constanst exists in my code

export const API_URL = 'https://api.coingecko.com/api/v3/coins/bitcoin/market_chart/range?vs_currency=eur&'
export const BITCOIN_MARKET_START_DATE = new Date('2013', '4', '1').getTime()
export const TIMEZONE_OFFSET = (new Date().getTimezoneOffset() / 60) * -1
export const HOUR_IN_SECONDS = 3600
export const DAY_IN_MILLISECONDS = 86400000
export const ADD_TIME_FROM = TIMEZONE_OFFSET * HOUR_IN_SECONDS
export const ADD_TIME_TO = ADD_TIME_FROM + 3600