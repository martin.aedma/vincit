import { DAY_IN_MILLISECONDS, TIMEZONE_OFFSET, BITCOIN_MARKET_START_DATE } from "./constants"

export const apiCall = async(url, method, headers, data ) => {
    const response = await fetch(url, {
      method: method,
      mode: 'cors',
      cache: 'no-cache',
      headers: headers ,
      body: JSON.stringify(data)
    })

    if(response.ok) {
        const answer = {...await response.json(), ok: true}
        return answer
    } else {
        const answer = {...await response.json(), ok: false}
        return answer
    }
}

export const validateDates = (dateFrom, dateTo) => {

    if(isNaN(dateFrom) || isNaN(dateTo)){
        return `Enter valid dates`
    }

    if(dateFrom < BITCOIN_MARKET_START_DATE){
        return `Dates start May 01 2013`
    }

    if(dateFrom > dateTo){
        return `Check date order`
    }

    if(dateTo > Date.now() - DAY_IN_MILLISECONDS){
        return `Cant look into future`
    }

    return true
}

export const calculateResults = (data) => {

    // if API response status was not ok
    if(data.ok === false) {
        return {ok: false}
    }

    // object to hold calculation result
    const result = {
        volume : {size : 0, date : 0},
        price : {},
        buy : 0,
        sell : 0,
        diff : 0,
        searchStart: 0,
        searchEnd: 0,
        ok: true
    }

    const volumes = [...data.total_volumes]
    const prices = [...data.prices]

    // depending on data granularity, if there is more than a single data point per day, this variable
    // holds single data point per day for price information
    const pricePerDay = []

    // variables to hold next possible downward trend and current longest downward trend
    const trendOngoing = {date: 0, price: 0, trend: 0}
    const trendLongest = {date: 0, price: 0, trend: 0}

    // at first data point initialize that day as first possible start day for downward trend
    trendOngoing.date = prices[0][0]
    trendOngoing.price = prices[0][1]

    // also set first data point as a price info for that day
    pricePerDay.push({date: prices[0][0], price: prices[0][1]})

    // API can return different amount of data points per prices / total_volumes
    // in such cases prices are longer array so they get iterated
    // when checking volume, and additional check is added to make sure that data point exists
    for(let i = 0; i < prices.length; i++){

        // get time difference between UTC midnight and current data point
        // use this to find data points close to midnight UTC
        const t = new Date(trendOngoing.date)
        const y = t.getHours()
        const x = t.getMinutes()
        const z = t.getSeconds()
        const diff = ((y-TIMEZONE_OFFSET)*3600000) + (x * 60000) + ( z * 1000)

        // check for highest volume
        if(volumes[i] && volumes[i][1] > result.volume.size){
            result.volume.size = volumes[i][1]
            result.volume.date = volumes[i][0]
        }

        // check price every day, close to midnight
        if(prices[i][0] >= trendOngoing.date + (DAY_IN_MILLISECONDS - diff)) {

            // check if price dropped compared to previous day
            if(prices[i][1] < trendOngoing.price){
                trendOngoing.trend = trendOngoing.trend + 1
            } else {
                trendOngoing.trend = 0
            }

            // update ongoing trend, date and price to current day
            trendOngoing.date = prices[i][0]
            trendOngoing.price = prices[i][1]

            // add this days datapoint to price per day
            pricePerDay.push({date: prices[i][0], price: prices[i][1]})
        }

        if(trendOngoing.trend > trendLongest.trend) {
            trendLongest = {...trendOngoing}
        }
    }

    // find optimal transaction dates using pirce per day array
    const priceDates = calculateOptimalTransactionDates(pricePerDay)

    // gather all calculated information and update result object for return
    result.price = {...trendLongest}
    result.buy = priceDates.buy
    result.sell = priceDates.sell
    result.diff = priceDates.diff
    result.searchStart = pricePerDay[0].date
    result.searchEnd = pricePerDay[pricePerDay.length-1].date

    return result
}

export const createUnixDate = (input) => {
    const arr = input.split('-')
    const date = new Date(arr[0], arr[1]-1, arr[2]).toUTCString()
    return Math.floor(Date.parse(date))
}


const calculateOptimalTransactionDates = (data) => {

    // variable to hold current best options for buy sell dates
    const currentBest = {buy: 0, sell: 0, diff: 0}

    // iterate all data points - days
    for (let i=0; i<data.length; i++){
        // will check each days data point with every other days data point
        // every new day (increased i) needs to be checked only with data points that come after it
        // because previous data points already compared themselves with current data point
        // thats why inner foor loop is initiated with j=i
        for(let j=i; j<data.length; j++){
            // look if data points are time wise correct and if price is cheaper
            if(data[j].date > data[i].date && data[j].price > data[i].price){
                const difference = data[j].price - data[i].price
                // if price difference between data points is higher than current best then there is new optimal buy / sell dates
                if( difference > currentBest.diff){
                    currentBest = {buy: data[i].date, sell: data[j].date, diff: difference }
                }
            }
        }
    }

    return currentBest
}

export const createViewableDate = (unix) => {
    const date = new Date(unix).toUTCString()
    const arr = date.slice(5, 16).split(' ')
    return `${arr[1]} ${arr[0]} ${arr[2]}`
}