import React from "react";

export const Footer = () => {

   return <div className='footer-container'>
        <a href='https://www.linkedin.com/in/martin-aedma-7380aa1a7/?originalSubdomain=fi' target='_blank' rel="noopener noreferrer" >
            <img
                src='/linked.svg'
                alt='link to martin aedma linked in profile'
                width={50}
                height={50}
            />
        </a>
    </div>
}