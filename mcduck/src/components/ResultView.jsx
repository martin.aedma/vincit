import React from "react";
import { DAY_IN_MILLISECONDS } from "../util/constants";
import { createViewableDate } from "../util/functions";

export const ResultView = (props) => {

    const formatPrice = (price) => {
        const formatter = new Intl.NumberFormat(
            'en-us', {
                style: 'currency',
                currency: 'EUR'
            }
        )
        return formatter.format(price)
    }

    return <div className='results-main-container'>

        <div className='results-column'>
            <div className='results-data header'>
                <h1>BITCOIN</h1>
                <h3>MARKET INFORMATION</h3>
                <h5>{createViewableDate(props.searchResult.searchStart).toUpperCase()} - {createViewableDate(props.searchResult.searchEnd).toUpperCase()}</h5>
            </div>
        </div>

        <div className='results-column'>
            <div className='results-data'>
                <div className='results-number'>
                    <img
                        src='/one.svg'
                        alt='first number'
                        width={50}
                        height={50}
                    />
                </div>
                <h4>LONGEST DOWNWARD TREND</h4>
                <span>{props.searchResult.price.trend !== 0 ? `${props.searchResult.price.trend} days`: 'None'}</span>
                {props.searchResult.price.trend !== 0 && <span>
                    {createViewableDate(props.searchResult.price.date - props.searchResult.price.trend * DAY_IN_MILLISECONDS )} --- {createViewableDate(props.searchResult.price.date)}
                </span>}
            </div>


            <div className='results-data'>
                <div className='results-number'>
                    <img
                        src='/two.svg'
                        alt='second number'
                        width={50}
                        height={50}
                    />
                </div>
                <h4>HIGHEST TRADING VOLUME</h4>

                {props.searchResult.volume.size !== 0 && <>
                    <span>{createViewableDate(props.searchResult.volume.date)}</span>
                    <span>{formatPrice(props.searchResult.volume.size)}</span>
                </>}

                {props.searchResult.volume.size === 0 && <span>Unknown</span>}

            </div>

            <div className='results-data'>
                <div className='results-number'>
                    <img
                        src='/three.svg'
                        alt='third number'
                        width={50}
                        height={50}
                    />
                </div>
                <h4>OPTIMAL TRANSACTION DATES</h4>
                {props.searchResult.buy !== props.searchResult.sell && <>
                    <span>Buy : {createViewableDate(props.searchResult.buy)}</span>
                    <span>Sell : {createViewableDate(props.searchResult.sell)}</span>
                </>}
                {props.searchResult.buy === props.searchResult.sell && <>
                    <span>None</span>
                </>}

            </div>
        </div>

        <div className='results-column'>
            <div className='results-data'>
                <a
                    className='btn'
                    onClick={() => props.setAnimOut({
                        trigger: true,
                        initiator: 3
                    })}>
                    <span>Return</span>
                </a>
            </div>
        </div>
    </div>
}