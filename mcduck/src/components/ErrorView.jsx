import React from "react";

export const ErrorView = (props) => {
    return <div className='scrooge-container'>
        <div className='results-column'>
            <div className='results-data'>
                <span>Something went wrong</span>
                <span>Try again?</span>
                <a
                    className='btn'
                    onClick={() => props.setAnimOut({
                        trigger: true,
                        initiator: 3
                    })}>
                    <span>Return</span>
                </a>
            </div>
        </div>
    </div>
}