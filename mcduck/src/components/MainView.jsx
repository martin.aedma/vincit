import React, { useState } from "react";
import { DatePicker } from "./DatePicker";
import { Scrooge } from "./Scrooge";
import { ResultView } from "./ResultView";
import { ErrorView } from "./ErrorView";

export const MainView = () => {

    const [searchResult, setSearchResult] = useState({ok: false})
    const [stateMachine, setStateMachine] = useState({
        isPickingDates: true,
        isCalculating: false,
        isViewingResult: false
    })
    const [animIn, setAnimIn] = useState(false)
    const [animOut, setAnimOut] = useState({trigger: false, initiator: 0 })

    const handleAnimations = () => {
        if(animOut.trigger){
            switch(animOut.initiator){
                case 1:
                    setStateMachine({isCalculating: true, isPickingDates:false, isViewingResult:false})
                    break;
                case 2:
                    setStateMachine({isCalculating: false, isPickingDates:false, isViewingResult:true})
                    break;
                case 3:
                    setStateMachine({isCalculating: false, isPickingDates:true, isViewingResult:false})
            }
            setAnimOut({trigger: false, initiator: 0})
            setAnimIn(true)
            return
        }

        if(animIn){
            setAnimIn(false)
        }
    }

    return <div className={ animIn ? 'root-container fade-in' : animOut.trigger ? 'root-container fade-out' : 'root-container'}
                onAnimationEnd={handleAnimations}
    >
        {stateMachine.isPickingDates && <DatePicker searchResult={searchResult} setSearchResult={setSearchResult} setAnimOut={setAnimOut}/>}
        {stateMachine.isCalculating && <Scrooge setAnimOut={setAnimOut}/>}
        {stateMachine.isViewingResult && searchResult.ok === true && <ResultView searchResult={searchResult} setAnimOut={setAnimOut}/>}
        {stateMachine.isViewingResult && searchResult.ok === false && <ErrorView setAnimOut={setAnimOut}/>}
    </div>
}