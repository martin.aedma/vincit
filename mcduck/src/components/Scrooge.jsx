import React, { useEffect } from 'react'

export const Scrooge = (props) => {

    const triggerAnimation = props.setAnimOut

    useEffect(() => {
        setTimeout(() => {
            triggerAnimation({trigger: true, initiator: 2})
        }, 2000)
    }, [triggerAnimation])

    return <div className='scrooge-container'>
        <img
            src='/mcduck.svg'
            alt='picture of scrooge mcduck'
            width={500}
            height={500}
        />
        <div className='loader'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
}