import React, { useEffect, useState } from "react";
import {apiCall, createUnixDate, calculateResults, validateDates} from "../util/functions";
import { API_URL, ADD_TIME_FROM, ADD_TIME_TO } from '../util/constants'
import { Footer } from "./Footer";

export const DatePicker = (props) => {

    const [fromDate, setFromDate] = useState('')
    const [toDate, setToDate] = useState('')
    const [error, setError] = useState(false)

    useEffect(() => {
        if(props.searchResult.previousFrom && props.searchResult.previousTo){
            setFromDate( props.searchResult.previousFrom)
            setToDate( props.searchResult.previousTo)
        }
    }, [props.searchResult])

    const handleChange = (e) => {
        const name = e.target.name
        const value = e.target.value

        switch(name){
            case 'from':
                setFromDate(value)
                break;
            case 'to':
                setToDate(value)
                break;
            default:
                console.log('this should not be seen @ DatePicker handleChange')
                break;
        }

        if(error !== false){
            setError(false)
        }
    }

    const handleKeyDown = (e) => {
        if(e.keyCode === 13){
           handleSubmit()
        }
    }

    const handleSubmit = async(e) => {

        const from = createUnixDate(fromDate)
        const to = createUnixDate(toDate)
        const validation = validateDates(from, to)

        if (validation !== true) {
            setError(validation)
            return
        }

        // Dates are ok!
        // convert millisecond unix time to seconds, which is needed for CoinGecko API
        const dates = {
            from : Math.floor(from/1000),
            to : Math.floor(to/1000)
        }

        // Trigger Scrooge waiting animation
         props.setAnimOut({trigger: true, initiator: 1})

        // Call API
        const apiAnswer = await apiCall(
            API_URL + `from=${dates.from + ADD_TIME_FROM}&to=${dates.to + ADD_TIME_TO}`,
            'Get',
            { 'Content-Type': 'application/json; charset=utf-8'}
        )

        const result = calculateResults(apiAnswer)
        props.setSearchResult({...result, previousFrom : fromDate, previousTo : toDate})
    }

    return <div className='datepicker-container'>
        <div className='ui-container'>

            <div className='results-column'>
                    <div className='results-data header'>
                        <h1>BITCOIN</h1>
                        <h3>MARKET INFORMATION</h3>
                    </div>
            </div>

            <span className={error !== false ? 'date-error-visible' : 'hidden'}>{error}</span>

            <label htmlFor='from'>From</label>
            <input
                type='date'
                name='from'
                id='from'
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                value={fromDate}
            />

            <label htmlFor='to'>To</label>
            <input
                type='date'
                name='to'
                id='to'
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                value={toDate}
            />

            <a className='btn' onClick={handleSubmit}>
                <span>Calculate</span>
            </a>

            <Footer/>

        </div>
    </div>
}